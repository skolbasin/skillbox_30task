import pytest


@pytest.mark.parametrize("route", ["/clients", "/clients/1"])
def test_get_endpoints(client, route, app):
    """
    Тест проверяет, что все GET-методы возвращают код 200.
    """
    resp = client.get(route)
    assert resp.status_code == 200


def test_create_client(client):
    """
    Тест проверяет создание клиента.
    """
    client_data = {
        "name": "Иван",
        "surname": "Иванов",
        "credit_card": "1234567890123456",
        "car_number": "А123ВС45",
    }
    resp = client.post("/clients", json=client_data)
    assert resp.status_code == 201


def test_create_parking(client):
    """
    Тест проверяет создание парковки.
    """
    parking_data = {"address": "Address", "opened": True, "count_places": 20}
    resp = client.post("/parkings", json=parking_data)
    assert resp.status_code == 201


@pytest.mark.parking
def test_check_in_parking(client):
    """
    Тест проверяет заезд на парковку.
    """
    check_in_data = {"client_id": 1, "parking_id": 1}
    resp = client.post("/client_parkings", json=check_in_data)
    assert resp.status_code == 200


@pytest.mark.parking
def test_check_out_parking(client):
    """
    Тест проверяет выезд с парковки.
    """
    check_out_data = {"client_id": 1, "parking_id": 1}
    resp = client.delete("/client_parkings", json=check_out_data)
    assert resp.status_code == 200
