from datetime import datetime

import pytest

from ..main.app import create_app
from ..main.app import db as _db
from ..main.models import Client, ClientParking, Parking


@pytest.fixture
def app():
    """Фикстура для запуска приложения"""
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()

        # Создание тестовых данных
        client = Client(
            name="Иван",
            surname="Иванов",
            credit_card="1234567890123456",
            car_number="А123ВС45",
        )
        parking = Parking(
            address="Some Address",
            opened=True,
            count_places=20,
            count_available_places=20,
        )
        client_parking = ClientParking(
            client_id=1, parking_id=1, time_in=datetime.now()
        )

        _db.session.add(client)
        _db.session.add(parking)
        _db.session.add(client_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    """Фикстура для работы с  запросами к приложению"""
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    """Фикстура для работы с БД"""
    with app.app_context():
        yield _db
