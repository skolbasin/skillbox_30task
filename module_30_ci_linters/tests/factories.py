import factory  # type: ignore
from factory.alchemy import SQLAlchemyModelFactory  # type: ignore
from factory.faker import Faker  # type: ignore

from ..main.app import db
from ..main.models import Client, Parking


class ClientFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name: Faker = Faker("first_name")
    surname: Faker = Faker("last_name")
    credit_card: Faker = Faker("credit_card_number")
    car_number: Faker = Faker("license_plate")


class ParkingFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address: Faker = Faker("address")
    opened: Faker = Faker("boolean")
    count_places: Faker = Faker("random_int", min=1, max=100)
    count_available_places: factory.LazyAttribute = factory.LazyAttribute(
        lambda obj: obj.count_places
    )
