from module_30_ci_linters.main.app import create_app

if __name__ == "__main__":
    app = create_app()
    app.run()
