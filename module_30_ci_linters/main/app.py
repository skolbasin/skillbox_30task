from datetime import datetime

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    # Создаем экземпляр приложения Flask
    app = Flask(__name__)
    # Устанавливаем путь к базе данных SQLite
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///parking.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    # Инициализируем расширение SQLAlchemy с отложенной инициализацией
    db.init_app(app)

    from .models import Client, ClientParking, Parking

    # создаем БД с имеющимися моделями из models.py
    with app.app_context():
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/clients", methods=["GET"])
    def get_clients():
        """
        Получить список всех клиентов.
        """
        clients = Client.query.all()
        clients_list = []
        for client in clients:
            client_data = {
                "id": client.id,
                "name": client.name,
                "surname": client.surname,
                "credit_card": client.credit_card,
                "car_number": client.car_number,
            }
            clients_list.append(client_data)
        return jsonify(clients_list)

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_client(client_id):
        """
        Получить информацию о клиенте по ID.
        """
        client = Client.query.get(client_id)
        if client:
            client_data = {
                "name": client.name,
                "surname": client.surname,
                "credit_card": client.credit_card,
                "car_number": client.car_number,
            }
            return jsonify(
                f"{client_data['name']} {client_data['surname']}. "
                f"Номер карты: {client_data['credit_card']}. "
                f"Номер авто: {client_data['car_number']}"
            )
        return jsonify({"message": "Клиент не найден"}), 404

    @app.route("/clients", methods=["POST"])
    def create_client():
        """
        Создать нового клиента.
        """
        data = request.json
        new_client = Client(
            name=data["name"],
            surname=data["surname"],
            credit_card=data["credit_card"],
            car_number=data["car_number"],
        )
        db.session.add(new_client)
        db.session.commit()
        return (
            jsonify(
                {
                    "message": f"Новый клиент {new_client.name} "
                    f"{new_client.surname} создан"
                }
            ),
            201,
        )

    @app.route("/parkings", methods=["POST"])
    def create_parking():
        """
        Создать новую парковочную зону.
        """
        data = request.json
        new_parking = Parking(
            address=data["address"],
            opened=data["opened"],
            count_places=data["count_places"],
            count_available_places=data["count_places"],
        )
        db.session.add(new_parking)
        db.session.commit()
        return (
            jsonify(
                {
                    "message": f"Новая парковочная зона №{new_parking.id} "
                    f"на {new_parking.count_places} мест создана"
                }
            ),
            201,
        )

    @app.route("/client_parkings", methods=["POST"])
    def check_in_parking():
        """
        Заезд на парковку.
        """
        data = request.json
        client_id = data["client_id"]
        parking_id = data["parking_id"]

        client_parking = ClientParking(
            client_id=client_id, parking_id=parking_id, time_in=datetime.now()
        )
        db.session.add(client_parking)

        parking = Parking.query.get(parking_id)
        # проверка на наличие свободных мест на парковке
        if parking.count_available_places > 0 and parking.opened:
            parking.count_available_places -= 1
            db.session.commit()
            current_time = datetime.now().strftime("%H:%M")
            return (
                jsonify(
                    {
                        "message": f"Заезд на парковку клиентом №{client_id} "
                        f"выполнен. Время: {current_time}"
                    }
                ),
                200,
            )
        else:
            return (
                jsonify({"message": "Парковка закрыта/нет свободных мест"}),
                400,
            )

    @app.route("/client_parkings", methods=["DELETE"])
    def check_out_parking():
        """
        Выезд с парковки.
        """
        data = request.json
        client_id = data["client_id"]
        parking_id = data["parking_id"]

        client_parking = ClientParking.query.filter_by(
            client_id=client_id, parking_id=parking_id
        ).first()
        if client_parking:
            db.session.delete(client_parking)

            parking = Parking.query.get(parking_id)
            parking.count_available_places += 1
            db.session.commit()

            return (
                jsonify(
                    {
                        "message": f"Выезд с парковки "
                        f"клиентом "
                        f"№ {client_id} выполнен"
                    }
                ),
                200,
            )
        else:
            return jsonify({"message": "Запись о заезде не найдена"}), 404

    return app
