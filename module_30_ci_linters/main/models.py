from sqlalchemy import CheckConstraint

from ..main.app import db

# class Base(metaclass=DeclarativeMeta):
#     __abstract__ = True


class Client(db.Model):  # type: ignore
    __tablename__ = "client"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(18))
    car_number = db.Column(db.String(10))

    __table_args__ = (
        CheckConstraint(
            "LENGTH(credit_card) >= 15 AND LENGTH(credit_card) <= 18",
            name="check_credit_card_length",
        ),
    )

    def __repr__(self):
        """
        Возвращает строковое представление объекта Client.
        """
        return f"Клиент {self.name} {self.surname}"


class Parking(db.Model):  # type: ignore
    __tablename__ = "parking"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        """
        Возвращает строковое представление объекта Parking.
        """
        return f"Парковка №{self.id}"


class ClientParking(db.Model):  # type: ignore
    __tablename__ = "client_parking"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey("client.id"))
    parking_id = db.Column(db.Integer, db.ForeignKey("parking.id"))
    time_in = db.Column(db.DateTime)
    time_out = db.Column(db.DateTime)

    def __repr__(self):
        """
        Возвращает строковое представление объекта ClientParking.
        """
        return f"Пара: Клиент {self.client_id}-Парковка {self.parking_id}"
